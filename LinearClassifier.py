import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import numpy as np
from sklearn.model_selection import train_test_split

from data_loader import data_preprocessing

"""
Hyperparameters: 
    num_steps: Number of steps
    batch_size: Training Batch size
    learning_rate: Learning rate for gradient descent
    threshold: Probability threshold for classification
    display_step: Period of steps between prints
"""
flags = tf.app.flags
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_float('learning_rate', 0.0035, """learning rate""")
tf.app.flags.DEFINE_integer('training_epochs', 5, """learning rate""")
tf.app.flags.DEFINE_integer('display_step', 10, """learning rate""")
tf.app.flags.DEFINE_integer('batch_size', 64, """learning rate""")
tf.app.flags.DEFINE_float('threshold', 0.5, """learning rate""")
tf.app.flags.DEFINE_float('train_size', 0.8, """learning rate""")



def get_data():

    df = data_preprocessing('./tensor-flow/weatherAUS.csv')
    X = df.iloc[:, df.columns != 'RainTomorrow']
    y = df['RainTomorrow']

    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=FLAGS.train_size, random_state=1)

    train_X = X_train.astype('float').to_numpy()
    train_Y = y_train.astype('float').to_numpy()
    test_X = X_test.astype('float').to_numpy()
    test_Y = y_test.astype('float').to_numpy()

    return train_X, train_Y, test_X, test_Y


def main():

    #Get data after pre-processing and split
    train_X, train_Y, test_X, test_Y = get_data()

    #Calculate relevant values for later
    num_examples, num_features = train_X.shape
    steps_per_epoch = int(num_examples / FLAGS.batch_size) + 1

    # tf Graph Input
    X = tf.placeholder("float64", shape=(None, num_features), name='X')
    Y = tf.placeholder("float64", shape=(None, 1), name='Y')
    # Set model weights and bias
    W = tf.Variable(np.random.rand(num_features, 1), name="weight", trainable=True)  # trainable=True by default
    b = tf.Variable(np.random.randn(), name="bias", dtype=tf.float64)


    # Construct a logistic regression model
    pred = tf.sigmoid(tf.add(tf.matmul(X, W), b))

    # Define a cost function for training (cross entropy)
    cost = tf.keras.losses.BinaryCrossentropy()(Y, pred)
    loss_summary = tf.summary.scalar('loss', cost)

    # Choose evaluation metrics
    y_pred = tf.cast(pred > FLAGS.threshold, tf.float64)
    y = tf.cast(Y, tf.float64)
    correct_pred = tf.equal(y_pred, y)
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float64))

    # Use simple momentum for the optimization.
    tf.train.create_global_step()
    optimizer = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(cost,
                                                                                global_step=tf.train.get_global_step())


    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()

    saver = tf.train.Saver()
    train_writer = tf.summary.FileWriter('./train', tf.get_default_graph())
    test_writer = tf.summary.FileWriter('./test')

    # Start training
    with tf.Session() as sess:

        # Run the initializer
        sess.run(init)

        # Fit all training data
        for epoch in range(FLAGS.training_epochs):
            print("Epoch #{} ".format(epoch))

            for step in range(steps_per_epoch):

                X_batch = train_X[step * FLAGS.batch_size: step * FLAGS.batch_size + FLAGS.batch_size]
                y_batch = train_Y[step * FLAGS.batch_size: step * FLAGS.batch_size + FLAGS.batch_size]

                _, loss_summ, global_step = sess.run([optimizer, loss_summary, tf.train.get_global_step()], feed_dict={X: X_batch, Y: y_batch.reshape(-1,1)})
                train_writer.add_summary(loss_summ, global_step)

                if (epoch + 1) % FLAGS.display_step == 0:
                    saver.save(sess, './model/', global_step=global_step)

            #Calc loss for the current epoch
            _, test_loss_summ, global_step = sess.run([optimizer, loss_summary, tf.train.get_global_step()],
                                                 feed_dict={X: test_X, Y: test_Y.reshape(-1, 1)})
            test_writer.add_summary(test_loss_summ, global_step)

        print("Optimization Finished!")

        training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y.reshape(-1, 1)})
        print("Training cost=", training_cost)

        saver.save(sess, './model/', global_step=epoch)

        print("Test Accuracy:", accuracy.eval({X: test_X, Y: test_Y.reshape(-1,1)}))

        saver.save(sess, './model/', global_step=epoch)

    train_writer.close()
    test_writer.close()








if __name__ == '__main__':
    main()

