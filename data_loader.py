import numpy as np
import pandas as pd
from scipy import stats
from sklearn import preprocessing

def data_preprocessing(f):
    """

    :param pd.Dataframe f:
    :return:
    """

    df = pd.read_csv(f)
    print('Size of weather data frame is :', df.shape)

    df = df.drop(columns=['Sunshine', 'Evaporation', 'Cloud3pm', 'Cloud9am', 'Location', 'RISK_MM', 'Date'], axis=1)

    df = df.dropna(how='any')

    z = np.abs(stats.zscore(df._get_numeric_data()))
    df = df[(z < 3).all(axis=1)]

    # Lets deal with the categorical cloumns now
    # simply change yes/no to 1/0 for RainToday and RainTomorrow
    df['RainToday'].replace({'No': 0, 'Yes': 1}, inplace=True)
    df['RainTomorrow'].replace({'No': 0, 'Yes': 1}, inplace=True)

    # See unique values and convert them to int using pd.getDummies()
    categorical_columns = ['WindGustDir', 'WindDir3pm', 'WindDir9am']
    # Transform the categorical columns
    df = pd.get_dummies(df, columns=categorical_columns)

    # Next step is to standardize our data - using MinMaxScaler
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df)
    df = pd.DataFrame(scaler.transform(df), index=df.index, columns=df.columns)

    return df